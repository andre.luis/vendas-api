package com.andreluis.vendas.services;

import com.andreluis.vendas.domain.Cidade;
import com.andreluis.vendas.domain.Estado;
import com.andreluis.vendas.repositories.CidadeRepository;
import com.andreluis.vendas.repositories.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadeService {

    @Autowired
    private CidadeRepository repo;

    public List<Cidade> findByEstado(Integer estadoId) {
        return repo.findCidades(estadoId);
    }
}
